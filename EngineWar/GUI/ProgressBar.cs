﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace EngineWar.GUI
{
    public class ProgressBar : Drawable
    {
        public ProgressBar(Rectangle dimensions, 
            string spriteSheet = GlobalVariables.DefaultContentName, 
            string font = GlobalVariables.DefaultContentName)
            :base(new List<string> { spriteSheet }, new List<string> { font }, new List<string>(), dimensions)
        {
            Done = 0;
        }

        public int Done { get; private set; }
        public bool IsDone => Done == 100;
        
        public void Add(int percents)
        {
            if (percents < 0)
                return;
            Done += percents;
            if (Done > 100)
                Done = 100;
        }

        public void Set(int percents)
        {
            if (percents < 0)
                return;
            Done = percents > 100 ? 100 : percents;
        }

        /// <summary>
        /// Does nothing
        /// </summary>
        internal override void Update() { }
    }
}