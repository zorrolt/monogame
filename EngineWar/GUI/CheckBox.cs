﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace EngineWar.GUI
{
    public class CheckBox : Drawable
    {
        private bool _isPushed;

        public CheckBox(Rectangle dimensions)
            :this(dimensions, GlobalVariables.DefaultContentName)
        {}

        public CheckBox(Rectangle dimensions, string spriteSheet)
            :base(new List<string> { spriteSheet }, new List<string>(), new List<string>(), dimensions)
        {
            IsSelected = false;
            _isPushed = false;
        }

        public bool IsSelected { get; private set; }

        public void SetSelect(bool selected)
        {
            IsSelected = selected;
        }

        internal override void Update()
        {
            if (Dimension.Contains(InputHandler.Instance.GetMouse()) && InputHandler.Instance.IsLeftMousePressed())
                _isPushed = true;
            else if (Dimension.Contains(InputHandler.Instance.GetMouse()))
            {
                if (_isPushed)
                    IsSelected = !IsSelected;
                _isPushed = false;
            }
            else if (!InputHandler.Instance.IsLeftMousePressed())
                _isPushed = false;
        }
    }
}