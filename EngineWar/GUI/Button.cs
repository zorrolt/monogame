﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace EngineWar.GUI
{
    public class Button : Drawable
    {
        public enum ButtonStates
        {
            Normal,
            Over,
            Pushed
        }

        private readonly Action _onClick;
        public string Text;

        public Button(Rectangle dimension, Action onClick, 
            string spriteSheet = GlobalVariables.DefaultContentName, string font = GlobalVariables.DefaultContentName)
            : this(dimension, "", onClick, spriteSheet, font)
        {}

        public Button(Rectangle dimension, string text, Action onClick, 
            string spriteSheet = GlobalVariables.DefaultContentName, string font = GlobalVariables.DefaultContentName)
            : base(new List<string> { spriteSheet }, new List<string> {font}, new List<string>(), dimension)
        {
            _onClick = onClick;
            State = ButtonStates.Normal;
            Text = text;
        }

        public ButtonStates State { get; set; }

        internal void TakeAction()
        {
            _onClick.Invoke();
        }

        internal override void Update()
        {
            if (Dimension.Contains(InputHandler.Instance.GetMouse()) && InputHandler.Instance.IsLeftMousePressed())
                State = ButtonStates.Pushed;
            else if (Dimension.Contains(InputHandler.Instance.GetMouse()))
            {
                if (State == ButtonStates.Pushed)
                    TakeAction();
                State = ButtonStates.Over;
            }
            else
                State = ButtonStates.Normal;
        }
    }
}