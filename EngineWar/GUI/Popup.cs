﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace EngineWar.GUI
{
    //TODO: improve text positioning
    public class Popup : Drawable
    {

        public Popup(string text, string title, Rectangle dimension, Color textColor, 
            string spriteSheet = GlobalVariables.DefaultContentName, 
            string closeButtonSpriteSheet = GlobalVariables.DefaultContentName,
            string font = GlobalVariables.DefaultContentName
            )
            :base(new List<string> {spriteSheet, closeButtonSpriteSheet }, new List<string> { font }, new List<string>(), dimension)
        {
            TextColor = textColor;
            Text = text;
            Title = title;
            Show = true;
            ExitButton = new Button(
                new Rectangle(dimension.Location.X + dimension.Size.X - 10,
                    dimension.Location.Y + 10,
                    10, 10), "", () => Show = false);
            ShowExitButton = true;
        }

        public string Text { get; private set; }
        public string Title { get; private set; }
        public Color TextColor { get; private set; }
        public Button ExitButton { get; }
        public bool ShowExitButton { get; protected set; }
        public bool Show { get; private set; }

        public void Toggle()
        {
            Show = !Show;
        }

        internal override void Update()
        {
            if (Show)
                ExitButton.Update();
        }
    }
}