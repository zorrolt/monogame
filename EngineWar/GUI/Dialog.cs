﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace EngineWar.GUI
{
    //TODO: refactor button positions, they should automaticly calculated acording layout strategy
    //TODO: Implement layout strategy
    //TODO: Make implementation with no title text(refactor Game.cs)
    public class Dialog : Popup
    {
        public readonly List<Button> Buttons;

        public Dialog(string text, string title, Rectangle dimension, List<Button> buttons, Color textColor, 
            string spriteSheet = GlobalVariables.DefaultContentName, 
            string closeButtonSpriteSheet = GlobalVariables.DefaultContentName,
            string font = GlobalVariables.DefaultContentName
            )
            : base(text, title, dimension, textColor, spriteSheet, closeButtonSpriteSheet, font)
        {
            Buttons = buttons;
            ShowExitButton = false;
        }

        internal override void Update()
        {
            base.Update();
            if (Show)
                foreach (var button in Buttons)
                    button.Update();
        }
    }
}