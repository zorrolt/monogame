﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace EngineWar.GUI
{
    public class TextBox : Drawable
    {
        private Rectangle _dimensions;
        public string Text;
        public readonly string Font;

        public TextBox(Rectangle dimensions, string text)
            : this(dimensions, text, GlobalVariables.DefaultContentName)
        {
        }

        public TextBox(Rectangle dimensions, string text, string font)
            : base(new List<string>(), new List<string> {font}, new List<string>(), dimensions)
        {
            _dimensions = dimensions;
            Text = text;
            Font = font;
        }

        public Rectangle GetDimension()
        {
            return _dimensions;
        }

        public Point GetLocation()
        {
            return _dimensions.Location;
        }

        public Point GetSize()
        {
            return _dimensions.Size;
        }

        public void SetDimension(Rectangle dimension)
        {
            _dimensions = dimension;
        }

        public void SetSize(Point size)
        {
            _dimensions.Size = size;
        }

        public void SetLocation(Point location)
        {
            _dimensions.Location = location;
        }

        /// <summary>
        /// Does nothing
        /// </summary>
        internal override void Update()
        {
        }
    }
}