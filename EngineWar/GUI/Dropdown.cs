﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace EngineWar.GUI
{
    public class Dropdown : Drawable
    {
        public enum States
        {
            Closed,
            Open
        }

        public readonly Button Button;
        public readonly Dictionary<string, Element> Items;
        public readonly Rectangle OpenDimensions;
        private bool _isPushed;

        public Dropdown(Dictionary<string, string> items, Rectangle dimensions, 
            string spriteSheet = GlobalVariables.DefaultContentName,
            string font = GlobalVariables.DefaultContentName)
            :base(new List<string> { spriteSheet }, new List<string> { font }, new List<string>(), 
                 new Rectangle(dimensions.Location.X + dimensions.Height, dimensions.Location.Y,
                dimensions.Width - dimensions.Height, dimensions.Height))
        {
            _isPushed = false;
            Button = new Button(new Rectangle(dimensions.X, dimensions.Y, dimensions.Height, dimensions.Height), "V",
                () => { State = State == States.Closed ? States.Open : States.Closed; });
            Rectangle dim = new Rectangle(dimensions.Location.X + dimensions.Height, dimensions.Location.Y,
                dimensions.Width - dimensions.Height, dimensions.Height);
            OpenDimensions = new Rectangle(dim.Location, dim.Size);
            OpenDimensions.Y += OpenDimensions.Height;
            OpenDimensions.Height = OpenDimensions.Height*items.Count;
            Items = new Dictionary<string, Element>();
            var i = 1;
            foreach (var item in items)
            {
                var pos = new Point(dim.X, dimensions.Height*i + dimensions.Y);
                Items.Add(item.Key, new Element {Value = item.Value, Dimensions = new Rectangle(pos, dim.Size)});
                i++;
            }
            Selected = Items.FirstOrDefault().Key;
        }

        public States State { get; private set; }
        public string Selected { get; private set; }

        internal override void Update()
        {
            Button.Update();
            // needed because Rectangle.Contains() is mutable method and OpenDimensions is readonly
            var temp = OpenDimensions;
            switch (State)
            {
                case States.Open:
                    if (temp.Contains(InputHandler.Instance.GetMouse()))
                    {
                        foreach (var item in Items)
                        {
                            if (item.Value.Dimensions.Contains(InputHandler.Instance.GetMouse()) &&
                                InputHandler.Instance.IsLeftMousePressed())
                                _isPushed = true;
                            else if (item.Value.Dimensions.Contains(InputHandler.Instance.GetMouse()))
                            {
                                if (_isPushed)
                                    Selected = item.Key;
                                _isPushed = false;
                            }
                        }
                    }
                    else
                    {
                        if (InputHandler.Instance.IsLeftMousePressed())
                            _isPushed = true;
                        else if (_isPushed && !temp.Contains(InputHandler.Instance.GetMouse()) &&
                                 !InputHandler.Instance.IsLeftMousePressed() &&
                                 !Dimension.Contains(InputHandler.Instance.GetMouse()) &&
                                 !Button.Dimension.Contains(InputHandler.Instance.GetMouse()))
                            State = States.Closed;
                        else
                            _isPushed = false;
                    }
                    break;
            }
        }

        public void Select(string label)
        {
            if (Items.ContainsKey(label))
                Selected = label;
        }

        public struct Element
        {
            public Rectangle Dimensions;
            public string Value;
        }
    }
}