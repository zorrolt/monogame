﻿using System;
using System.Collections.Generic;
using System.Linq;
using EngineWar.DataEntities;
using EngineWar.Properties;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace EngineWar
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class GameWar : Game, IEngine
    {
        readonly GraphicsDeviceManager _graphics;
        SpriteBatch _spriteBatch;
        private Dictionary<string, Point> _supportedResolutions;

        public GameWar()
        {
            _graphics = new GraphicsDeviceManager(this);
            ScreenManager.Instance.Engine = this;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            using (var reader = new DataReader(GlobalVariables.SettingsFilePath))
            {
                var settings = reader.Get<Settings>();
                _graphics.PreferredBackBufferWidth = settings.DefaultResolution.Width;
                _graphics.PreferredBackBufferHeight = settings.DefaultResolution.Height;
                Content.RootDirectory = settings.GameContentPath;
            }
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            ScreenManager.Instance.Renderer = new Renderer(_spriteBatch);
            ScreenManager.Instance.SetContent(Content);
            _graphics.ApplyChanges();
            InputHandler.Instance.SetGraphicDevice(_graphics);
            _supportedResolutions =
                GraphicsAdapter.DefaultAdapter.SupportedDisplayModes.ToDictionary(
                    mode => mode.Width + "X" + mode.Height, mode => new Point(mode.Width, mode.Height));
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            ScreenManager.Instance.LoadContent(new ResourceContentManager(Services, Resources.ResourceManager));
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            ScreenManager.Instance.Update(gameTime);
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            var matrix = Matrix.CreateScale(
                _graphics.PreferredBackBufferWidth / ScreenManager.Instance.Dimensions.X,
                _graphics.PreferredBackBufferHeight / ScreenManager.Instance.Dimensions.Y, 1.0f);
            _spriteBatch.Begin(transformMatrix: matrix);
            ScreenManager.Instance.Draw();
            _spriteBatch.End();
            base.Draw(gameTime);
        }

        public Dictionary<string, Point> GetSupportedResolutions()
        {
            return _supportedResolutions;
        }

        public void ChangeResolution(string resolution, bool isFullScreen)
        {
            if (!_supportedResolutions.ContainsKey(resolution))
                throw EngineErrors.UnsupportedResolutionException;
            _graphics.PreferredBackBufferWidth = _supportedResolutions[resolution].X;
            _graphics.PreferredBackBufferHeight = _supportedResolutions[resolution].Y;
            _graphics.IsFullScreen = isFullScreen;
            _graphics.ApplyChanges();
        }

        public Point GetResolution()
        {
            return new Point(_graphics.PreferredBackBufferWidth, _graphics.PreferredBackBufferHeight);
        }

        public void Close()
        {
            Exit();
        }

        public bool IsFullScreen()
        {
            return _graphics.IsFullScreen;
        }
    }
}