﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace EngineWar
{
    public class DataReader : IDisposable
    {
        private StreamReader _file;
        public bool IsOpen { get; private set; }

        public DataReader()
        {
            
        }

        public DataReader(string filePath)
        {
            OpenFile(filePath);
        }

        public void OpenFile(string filePath)
        {
            if(IsOpen)
                Close();
            _file = new StreamReader(filePath);
            IsOpen = true;
        }

        public void Close()
        {
            if(IsOpen)
                _file.Close();
        }

        public T Get<T>()
        {
            if(!IsOpen)
                throw EngineErrors.FileNotOpenException;
            return JsonConvert.DeserializeObject<T>(_file.ReadToEnd());
        }

        public void Dispose()
        {
            Close();
        }
    }
}
