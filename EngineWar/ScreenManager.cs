﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace EngineWar
{
    public class ScreenManager
    {
        private static ScreenManager _instance;
        private string _currentScreen;
        private readonly Dictionary<string, Screen> _screens;
        internal ContentManager Content;
        public bool IsMouseEnabled;
        private Texture2D _mouse;
        internal DefaultContentManager DefaultContent { get; private set; }

        private ScreenManager()
        {
            _screens = new Dictionary<string, Screen>();
            Dimensions = new Vector2(GlobalVariables.VirtualDimesnionsWidth, GlobalVariables.VirtualDimesnionsHeight);
            IsMouseEnabled = true;
        }

        public IEngine Engine { get; set; }
        private Renderer _renderer;

        internal Renderer Renderer
        {
            get { return _renderer; }
            set
            {
                _renderer = value;
                SetScreen(_currentScreen);
            }
        }

        public static ScreenManager Instance => _instance ?? (_instance = new ScreenManager());
        public Vector2 Dimensions { get; private set; }

        public void SetScreen(string name)
        {
            if (_screens.ContainsKey(name))
            {
                var current = _screens[name];
                Renderer?.SetContent(current.Textures, current.SpriteSheets, current.Fonts);
                _currentScreen = name;
            }
        }

        public void AddScreen(Screen screen)
        {
            if (!_screens.ContainsKey(screen.Name))
                _screens.Add(screen.Name, screen);
        }

        internal void Update(GameTime gameTime)
        {
            if (!string.IsNullOrEmpty(_currentScreen))
                _screens[_currentScreen].Update(gameTime);
        }

        internal void Draw()
        {
            Renderer.Draw(DefaultContent.Background);
            if (!string.IsNullOrEmpty(_currentScreen))
                _screens[_currentScreen].Draw();
            if (IsMouseEnabled)
                Renderer.Draw(_mouse ?? DefaultContent.Mouse,
                    new Rectangle(InputHandler.Instance.GetMouse(), new Point(25, 25)));
        }

        internal void SetContent(ContentManager content)
        {
            Content = content;
        }

        internal void LoadContent(ContentManager internalContent)
        {
            DefaultContent = new DefaultContentManager(internalContent);
            DefaultContent.LoadContent();
        }
    }
}