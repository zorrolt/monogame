﻿namespace EngineWar
{
    public static class GlobalVariables
    {
        internal const string MousePointer = "DefaultMouse";
        internal const string Background = "DefaultBackground";
        internal const string LoadingBar = "DefaultLoadingBar";
        internal const string SettingsFilePath = "Settings.json";
        internal const string DefaultContentName = "default";
        internal const string DefaultLoadingScreenName = "DefaultLoading";
        internal const string DefaultFont = "DefaultFont";

        public const int VirtualDimesnionsWidth = 1920;
        public const int VirtualDimesnionsHeight = 1080;
    }
}