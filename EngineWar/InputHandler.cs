﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace EngineWar
{
    internal class InputHandler
    {
        private static InputHandler _instance;
        private GraphicsDeviceManager _graphics;

        private InputHandler()
        {
        }

        internal static InputHandler Instance => _instance ?? (_instance = new InputHandler());

        internal void SetGraphicDevice(GraphicsDeviceManager graphic)
        {
            _graphics = graphic;
        }

        internal Point GetMouse()
        {
            var state = Mouse.GetState();
            var matrix = Matrix.CreateScale(
                _graphics.PreferredBackBufferWidth/ScreenManager.Instance.Dimensions.X,
                _graphics.PreferredBackBufferHeight/ScreenManager.Instance.Dimensions.Y, 1.0f);
            var scale = new Vector2(matrix.M11, matrix.M22);
            return (state.Position.ToVector2()/scale).ToPoint();
        }

        internal bool IsLeftMousePressed()
        {
            return Mouse.GetState().LeftButton == ButtonState.Pressed;
        }
    }
}