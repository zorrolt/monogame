﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace EngineWar
{
    public class Picture : Drawable
    {
        public Picture(string texture)
            : this(
                new Rectangle(0, 0, GlobalVariables.VirtualDimesnionsWidth, GlobalVariables.VirtualDimesnionsHeight),
                texture)
        {

        }

        public Picture(Rectangle dimension, string texture)
            : base(new List<string>(), new List<string>(), new List<string> {texture}, dimension)
        {
        }

        /// <summary>
        /// Does nothing
        /// </summary>
        internal override sealed void Update()
        {
        }
    }
}