﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace EngineWar
{
    public interface IEngine
    {
        void Exit();
        void ChangeResolution(string resolution, bool isFullScreen);
        Point GetResolution();
        Dictionary<string, Point> GetSupportedResolutions(); 
        bool IsFullScreen();
    }
}