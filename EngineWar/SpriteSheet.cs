﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EngineWar
{
    public class SpriteSheet
    {
        public readonly Rectangle[] Frames;
        public readonly Texture2D Texture;

        public SpriteSheet(Texture2D texture, Rectangle frameSize)
        {
            Texture = texture;
            //TODO: move exception to errors class
            if (texture.Bounds.Height%frameSize.Height != 0 || texture.Bounds.Height%frameSize.Height != 0)
                throw new Exception("Wrong frame size.");
            Frames = new Rectangle[(texture.Bounds.Height/frameSize.Height)*(texture.Bounds.Height/frameSize.Height)];
            for (var i = 0; i < (texture.Bounds.Height/frameSize.Height); i++)
            {
                for (var j = 0; j < (texture.Bounds.Width/frameSize.Width); j++)
                {
                    Frames[i] = new Rectangle(j*frameSize.Width, i*frameSize.Height, frameSize.Width, frameSize.Height);
                }
            }
        }

        public int FramesCount => Frames.Length;
    }
}