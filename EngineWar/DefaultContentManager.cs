﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace EngineWar
{
    internal class DefaultContentManager
    {
        internal Texture2D Mouse, Background, LoadingBar;
        internal SpriteFont Font;
        private readonly ContentManager _internalContentManger;

        internal DefaultContentManager(ContentManager internalContentManger)
        {
            _internalContentManger = internalContentManger;
        }

        internal void LoadContent()
        {
            Mouse = _internalContentManger.Load<Texture2D>(GlobalVariables.MousePointer);
            Background = _internalContentManger.Load<Texture2D>(GlobalVariables.Background);
            LoadingBar = _internalContentManger.Load<Texture2D>(GlobalVariables.LoadingBar);
            Font = _internalContentManger.Load<SpriteFont>(GlobalVariables.DefaultFont);
        }

        internal void UnloadContent()
        {
            _internalContentManger.Unload();
        }

    }
}
