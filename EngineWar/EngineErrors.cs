﻿using System;

namespace EngineWar
{
    internal static class EngineErrors
    {
        internal static Exception FileNotOpenException =
            new Exception
                ("File is not open.");

        internal static Exception UnsupportedResolutionException =
            new Exception
                ("Unsupported resolution.");
    }
}