﻿using System.Collections.Generic;
using System.Drawing;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Rectangle = Microsoft.Xna.Framework.Rectangle;

namespace EngineWar
{
    public abstract class Screen
    {
        public enum Layers
        {
            Ground,
            Third,
            Second,
            First,
            Top
        }
        
        public readonly string Name;
        private Picture _background;
        public string Background {
            get
            {
                if (_background != null) return _background?.Textures[0];
                return "";
            }
            set { _background = new Picture(Textures.ContainsKey(value) ? value : GlobalVariables.DefaultContentName); }
        }
        internal bool IsLoaded;
        internal readonly int TotalResources;
        internal int LodadedResources { get; private set; }
        internal protected ContentManager Content;
        internal Dictionary<string, Texture2D> Textures;
        internal Dictionary<string, SpriteSheet> SpriteSheets;
        internal Dictionary<string, SpriteFont> Fonts;
        private readonly Dictionary<Layers, List<Drawable>> _layers;

        internal void Draw()
        {
            Draw(Layers.Ground);
            Draw(Layers.Third);
            Draw(Layers.Second);
            Draw(Layers.First);
            Draw(Layers.Top);
        }

        private void Draw(Layers layer)
        {
            foreach (var element in _layers[layer])
            {
               ScreenManager.Instance.Renderer.Draw(element);
            }
        }

        internal void Update(GameTime gameTime)
        {
            Update(Layers.Ground);
            Update(Layers.Third);
            Update(Layers.Second);
            Update(Layers.First);
            Update(Layers.Top);
        }

        private void Update(Layers layer)
        {
            foreach (var element in _layers[layer])
                element.Update();
        }

        internal protected struct SpriteSheetTemplate
        {
            public Rectangle Dimension;
            public string Texture;
        }

        //TODO: add loading
        internal protected Screen(string name)
        {
            _layers = new Dictionary<Layers, List<Drawable>>
            {
                {Layers.Ground, new List<Drawable>()},
                {Layers.Third, new List<Drawable>()},
                {Layers.Second, new List<Drawable>()},
                {Layers.First, new List<Drawable>()},
                {Layers.Top, new List<Drawable>()}
            };
            Textures = new Dictionary<string, Texture2D>();
            SpriteSheets = new Dictionary<string, SpriteSheet>();
            Fonts = new Dictionary<string, SpriteFont>();
            TotalResources = 0;
            LodadedResources = 0;
            Name = name;
        }

        internal void LoadContent()
        {
            //TODO: explicitly assign root directory for each screen
            Content = new ContentManager(ScreenManager.Instance.Content.ServiceProvider, Content.RootDirectory);
            IsLoaded = true;
        }

        internal int Progress => (int)(((float)LodadedResources)/TotalResources)*100;

        internal void ResetProgress()
        {
            LodadedResources = 0;
        }

        internal void UnloadContent()
        {
            Content.Unload();
            IsLoaded = false;
        }
    }
}