﻿using Microsoft.Xna.Framework;

namespace EngineWar.DataEntities
{
    internal struct Settings
    {
        public string GameContentPath { set; get; }
        public Rectangle DefaultResolution { set; get; }
    }
}
