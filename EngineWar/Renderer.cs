﻿using System.Collections.Generic;
using EngineWar.GUI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EngineWar
{
    internal class Renderer
    {
        private readonly SpriteBatch _sp;

        private Dictionary<string, Texture2D> _textures;
        private Dictionary<string, SpriteSheet> _spriteSheets;
        private Dictionary<string, SpriteFont> _fonts;

        internal void SetContent(Dictionary<string, Texture2D> textures, Dictionary<string, SpriteSheet> spriteSheets, Dictionary<string, SpriteFont> fonts)
        {
            _textures = textures;
            _spriteSheets = spriteSheets;
            _fonts = fonts;
        }

        internal void CleanConten()
        {
            _textures = new Dictionary<string, Texture2D>();
            _spriteSheets = new Dictionary<string, SpriteSheet>();
            _fonts = new Dictionary<string, SpriteFont>();
        }

        /// <summary>
        ///     Set spritebatch for renderer.
        /// </summary>
        /// <param name="spriteBatch">Spritebatch.</param>
        internal Renderer(SpriteBatch spriteBatch)
        {
            _sp = spriteBatch;
        }

        //TODO: refactor for all
        internal void Draw(Drawable element)
        {
            
        }

        //TODO: refacrtor hardcoded values, fix popup top boarder problem(too small)
        /// <summary>
        ///     Draws popup.
        /// </summary>
        /// <param name="popup">Popup object.</param>
        /// <param name="background">Popup background</param>
        /// <param name="top">Popup top border/header texture.</param>
        /// <param name="button">SpriteSheet for dialog buttons.</param>
        /// <param name="font">Font for dialog buttons and text.</param>
        internal void Draw(Popup popup, Texture2D background, Texture2D top, SpriteSheet button, SpriteFont font)
        {
            if (popup.Show)
            {
               /* var topDim = popup.GetDimension();
                var bodyDim = popup.GetDimension();
                bodyDim.Location = new Point(bodyDim.Location.X, bodyDim.Location.Y + 30);
                bodyDim.Size = new Point(bodyDim.Size.X - 20, bodyDim.Size.Y - 40);
                topDim.Size = new Point(topDim.Size.X, 20);
                Draw(background, popup.GetDimension());
                Draw(background, topDim);
                Draw(font, popup.Title, topDim, popup.TextColor);
                Draw(font, popup.Text, bodyDim, popup.TextColor);
                if (popup.ShowExitButton)
                    Draw(popup.ExitButton, button, font);*/
            }
        }

        /// <summary>
        ///     Draws dialog.
        /// </summary>
        /// <param name="dialog">Dialog object.</param>
        /// <param name="background">Dialog background texture.</param>
        /// <param name="top">Dialog top border/header texture.</param>
        /// <param name="buttonSprite">SpriteSheet for dialog buttons.</param>
        /// <param name="font">Font for dialog buttons and text.</param>
        internal void Draw(Dialog dialog, Texture2D background, Texture2D top, SpriteSheet buttonSprite, SpriteFont font)
        {
            if (dialog.Show)
            {
                Draw((Popup) dialog, background, top, buttonSprite, font);
                foreach (var button in dialog.Buttons)
                    Draw(button, buttonSprite, font);
            }
        }

        /// <summary>
        ///     Draws simple picture.
        /// </summary>
        /// <param name="picture">Picture object.</param>
        /// <param name="texture">Texture.</param>
        internal void Draw(Picture picture, Texture2D texture)
        {
            //Draw(texture, picture.GetDimension());
        }

        /// <summary>
        ///     Draws texture on fullscreen.
        /// </summary>
        /// <param name="texture">Texture.</param>
        internal void Draw(Texture2D texture)
        {
            _sp.Draw(texture,
                new Rectangle(0, 0, (int) ScreenManager.Instance.Dimensions.X, (int) ScreenManager.Instance.Dimensions.Y),
                Color.White);
        }

        /// <summary>
        ///     Draws checkbox.
        /// </summary>
        /// <param name="checkbox">Checkbox object.</param>
        /// <param name="sprite">SpriteSheet for checkbox (selected and not).</param>
        internal void Draw(CheckBox checkbox, SpriteSheet sprite)
        {
            //Draw(sprite.Texture, checkbox.IsSelected ? sprite.Frames[1] : sprite.Frames[0], checkbox.GetDimension());
        }

        /// <summary>
        ///     Draws dropdown.
        /// </summary>
        /// <param name="dropdown">Dropdown object.</param>
        /// <param name="button">Dropdown expand button.</param>
        /// <param name="background">Options list background SpriteSheet (selected and normal item background).</param>
        /// <param name="font">Option label font.</param>
        internal void Draw(Dropdown dropdown, SpriteSheet button, SpriteSheet background, SpriteFont font)
        {
            /*Draw(background.Texture, background.Frames[(int) Dropdown.States.Closed], dropdown.GetDimension());
            switch (dropdown.State)
            {
                case Dropdown.States.Closed:
                    Draw(font, dropdown.Selected, dropdown.GetDimension(), Color.LightGray);
                    break;
                case Dropdown.States.Open:
                    Draw(font, dropdown.Selected, dropdown.GetDimension(), Color.Black);
                    foreach (var item in dropdown.Items)
                    {
                        if (item.Key == dropdown.Selected)
                        {
                            Draw(background.Texture, background.Frames[(int) Dropdown.States.Open],
                                item.Value.Dimensions);
                            Draw(font, item.Key, item.Value.Dimensions, Color.GreenYellow);
                        }
                        else
                        {
                            Draw(background.Texture, background.Frames[(int) Dropdown.States.Closed],
                                item.Value.Dimensions);
                            Draw(font, item.Key, item.Value.Dimensions, Color.White);
                        }
                    }
                    break;
            }
            Draw(dropdown.Button, button, font);*/
        }

        /// <summary>
        ///     Draws image in selected screen region. Image will be stretched or pressed.
        /// </summary>
        /// <param name="texture">Texture</param>
        /// <param name="dimensions">Screen region.</param>
        internal void Draw(Texture2D texture, Rectangle dimensions)
        {
            _sp.Draw(texture, dimensions, Color.White);
        }

        /// <summary>
        ///     Draws only part of a texture (crop) in selected screen part. Image will be stretched or pressed.
        /// </summary>
        /// <param name="texture">Texture.</param>
        /// <param name="frame">Crop frame.</param>
        /// <param name="dimensions">Spot on the screen where texture should be drawn.</param>
        internal void Draw(Texture2D texture, Rectangle frame, Rectangle dimensions)
        {
            _sp.Draw(texture, dimensions, frame, Color.White, 0, new Vector2(0, 0), SpriteEffects.None, 0);
        }

        /// <summary>
        ///     Draws button.
        /// </summary>
        /// <param name="button">Buttons object.</param>
        /// <param name="sheet">Button SpriteSheet texture.</param>
        /// <param name="font">Font for button text.</param>
        internal void Draw(Button button, SpriteSheet sheet, SpriteFont font)
        {
            /*var b = (int) button.State;
            var c = Color.White;
            if (b == 1)
                c = Color.Yellow;
            else if (b == 2)
                c = Color.Red;
            Draw(sheet.Texture, sheet.Frames[b], button.GetDimension());
            Draw(font, button.Text, button.GetDimension(), c);*/
        }

        /// <summary>
        ///     Custom button with no text. Draws only spriteSheet texture (text should be in texture or it could be only an
        ///     image). Button text in a button object will be ignored.
        /// </summary>
        /// <param name="button">Button object.</param>
        /// <param name="sheet">Button SpriteSheet texture.</param>
        internal void Draw(Button button, SpriteSheet sheet)
        {
           /* var b = (int) button.State;

            if (b == 2)
                Draw(sheet.Texture, sheet.Frames[1],
                    new Rectangle((int) (button.GetLocation().X + (button.GetSize().X*0.3)/2),
                        (int) (button.GetLocation().Y + (button.GetSize().Y*0.3)/2), (int) (button.GetSize().X*0.7),
                        (int) (button.GetSize().X*0.7)));
            else
                Draw(sheet.Texture, sheet.Frames[b], button.GetDimension());*/
        }

        internal void Draw(SpriteFont font, Color color, TextBox textBox)
        {
            Draw(font, textBox.Text, textBox.GetDimension(), color);
        }

        /// <summary>
        ///     Draws text in rectangle.
        /// </summary>
        /// <param name="font">Text font.</param>
        /// <param name="text">Text.</param>
        /// <param name="bounds">Rectangle where text should be placed.</param>
        /// <param name="color">Text color.</param>
        /// <param name="scale"></param>
        internal void Draw(SpriteFont font, string text, Rectangle bounds, Color color, float scale = 1f)
        {
            var size = font.MeasureString(text);
            var pos = bounds.Center.ToVector2();
            var origin = size*0.5f;
            _sp.DrawString(font, text, pos, color, 0, origin, scale, SpriteEffects.None, 0);
        }

        /// <summary>
        ///     Draws progress bar. Text color - white, so use spriteSheet with dark colours or refactor this method.
        /// </summary>
        /// <param name="bar">Progress bar object.</param>
        /// <param name="font">Progress precentage font.</param>
        /// <param name="sprite">SpriteSheet texture for progress bar.</param>
        internal void Draw(ProgressBar bar, SpriteFont font, SpriteSheet sprite)
        {
            /*Draw(sprite.Texture, sprite.Frames[0], bar.GetDimension());
            Draw(sprite.Texture,
                new Rectangle(sprite.Frames[1].Location,
                    new Point(sprite.Frames[1].Width*bar.Done/100, sprite.Frames[1].Height)),
                new Rectangle(bar.GetLocation(), new Point(bar.GetSize().X*bar.Done/100, bar.GetSize().Y)));
            Draw(font, bar.Done + " %",
                new Rectangle(bar.GetLocation().X, bar.GetLocation().Y, bar.GetSize().X, bar.GetSize().Y), Color.White,
                0.8f);*/
        }
    }
}