﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace EngineWar
{
    public abstract class Drawable
    {
        public readonly List<string> SpriteSheet;
        public readonly List<string> Fonts;
        public readonly List<string> Textures;
        public Rectangle Dimension { get; set; }

        protected Drawable(List<string> spriteSheets, List<string> fonts, List<string> textures, Rectangle dimension)
        {
            SpriteSheet = spriteSheets;
            Fonts = fonts;
            Textures = textures;
            Dimension = dimension;
        }

        public Vector2 Position => Dimension.Location.ToVector2();

        internal abstract void Update();
    }
}