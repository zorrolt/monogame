﻿using EngineWar;

namespace TestGame
{
    class Program
    {
        static void Main(string[] args)
        {
            var menuSrc = new MenuScreen();
            ScreenManager.Instance.AddScreen(menuSrc);
            ScreenManager.Instance.SetScreen("menu");
            var game = new GameWar();
            game.Run();
        }
    }
}
